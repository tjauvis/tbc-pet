import {resolve} from 'path'
import mpa from 'vite-plugin-mpa'
import handlebars from 'vite-plugin-handlebars';
import {defineConfig} from "vite";
import { menuItems } from "./helpers/menuItems";


/** @type {import('vite').UserConfig} */
export default defineConfig({
    plugins: [
        mpa(),
        handlebars({
            partialDirectory: resolve(__dirname, 'src', 'partials'),
            context(pagePath) {
                return {
                    menuItems: menuItems(pagePath.replace('src/pages/', '').replace('/index.html', '')),
                }
            }
        })
    ]
})