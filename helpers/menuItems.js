export const menuItems = (pagePath) => ([
    {
        title: 'Главная',
        href: '/#',
        isActive: pagePath === 'index'
    },
    {
        title: 'Продукты',
        href: '/products#',
        isActive: pagePath === 'products'
    },
    {
        title: 'События',
        href: '/world-events#',
        isActive: pagePath === 'world-events'
    },
    {
        title: 'Блог',
        href: '/blog#',
        isActive: pagePath === 'blog'
    },
    {
        title: 'Продакшн',
        href: '/production#',
        isActive: pagePath === 'production'
    },
])