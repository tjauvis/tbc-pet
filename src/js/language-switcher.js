const langToggle = document.getElementById('lang-toggle')
const popUp = document.getElementById('hide')

const menuBurger = document.getElementById('nav-right-burger')
const dropDown = document.getElementById('nav-dropdown-menu')

const navWrapperEl = document.getElementById('navWrapperEl')

langToggle.addEventListener('click', () => {
    popUp.classList.toggle('hide')
})

menuBurger.addEventListener('click', () => {

    menuBurger.classList.toggle('opened')
    dropDown.classList.toggle('opened')
})